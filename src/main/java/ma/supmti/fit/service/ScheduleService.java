package ma.supmti.fit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import ma.supmti.fit.model.Course;
import ma.supmti.fit.model.Schedule;
import ma.supmti.fit.model.User;
import ma.supmti.fit.repository.ScheduleRepository;

@Service
public class ScheduleService {
	
	@Autowired
	private ScheduleRepository scheduleRepository;
	

	public Schedule findById (long id) {
		return scheduleRepository.findOne(id);
	}
	
	public boolean deleteSchedule(long id) {
		Schedule s = scheduleRepository.findOne(id);
		if (s != null) {
			scheduleRepository.delete(id);
			return true;
		}
		return false;
	}
	
	public List<Schedule> getAllSchedule() {
		return scheduleRepository.findAll();
	}
	
	public void save(Schedule schedule) {
		scheduleRepository.save(schedule);
	}
	
	/**
	 * Join an Schedule 
	 * @param user
	 * @param schedule
	 */
	
	public void join(User user, Schedule schedule) {
		if(schedule.getUser().contains(user)) {
			schedule.deleteUser(user);
		}else {
			schedule.addUser(user);
		}
	}

	//Pagination
	public Page<Schedule> findAllSchedules(PageRequest page) {
		return scheduleRepository.findAll(page);
	}
}
