package ma.supmti.fit.model;

public enum Category {
	STRENGTH,
	CARDIO,
	FREESTYLE,
	DANCE,
	MIND
}
