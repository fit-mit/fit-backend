package ma.supmti.fit.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import ma.supmti.fit.model.Facilities;

@Repository
public interface FacilitiesRepository extends PagingAndSortingRepository<Facilities, Long> {
	Page<Facilities> findAll(Pageable pageable);

}
