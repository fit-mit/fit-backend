package ma.supmti.fit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.supmti.fit.model.User;

@Repository
public interface UserRepository extends JpaRepository <User, Long>{
	
	User findByNickname(String nickname);
	
	User findByEmail(String email);
	
	User findById(long id);
	
}
