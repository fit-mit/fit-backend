
package ma.supmti.fit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.supmti.fit.model.Schedule;
import ma.supmti.fit.model.User;

import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository <Schedule, Long>{

	public List<Schedule> findByListUsersContains(User u);


}
